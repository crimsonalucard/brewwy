from django.conf.urls import patterns, include, url
from main.views import front, register, login, registration_success
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'brewwy.views.home', name='home'),
    # url(r'^brewwy/', include('brewwy.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:

    url(r'^$',front, name = 'home'),
    url(r'^register/$', register, name = 'register'),
    url(r'^login/$',login, name = 'login'),
    url(r'^registration_success/$', registration_success, name = 'registration_success'),


    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

)

urlpatterns+=staticfiles_urlpatterns()
