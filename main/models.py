from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import datetime


class Location(models.Model):
    location = models.CharField(max_length = 100)
    def __unicode__(self):
        return unicode(self.location)

class VenueType(models.Model):
    venueType = models.CharField(max_length = 100)
    def __unicode__(self):
        return unicode(self.venueType)


class Venue(models.Model):
    name = models.CharField(max_length = 100, verbose_name='venue name')
    email = models.EmailField(max_length=100)
    phone_number = models.PositiveSmallIntegerField(max_length=10)
    picture = models.ImageField(upload_to='/venue', blank=True)
    description = models.TextField()

    street = models.CharField(max_length= 100, blank=True)
    city = models.CharField(max_length= 100, blank=True)
    state = models.CharField(max_length= 100, blank=True)
    zip = models.PositiveSmallIntegerField(max_length=5, blank=True)

    location = models.ForeignKey(Location)
    venueType = models.ForeignKey(VenueType)



    def numberOfDealsCreated(self):
        #implement later
        return False

    def numberOfDealsSold(self):
        #implement later
        return False

    def __unicode__(self):
        return unicode(self.name)

class Deal(models.Model):
    name = models.CharField(max_length=100)
    original_price = models.DecimalField(max_digits = 12, decimal_places=2)
    deal_price = models.DecimalField(max_digits = 12, decimal_places=2)
    date_created = models.DateTimeField()
    date_deal_ends = models.DateTimeField()
    picture_of_deal = models.ImageField(upload_to='/deal/main', blank = True)
    venue = models.ForeignKey(Venue)

    def timeLeftOnDeal(self):
        now = timezone.now()
        if now > self.date_deal_ends:
            return datetime.timedelta(days = 0)
        else:
            return self.date_deal_ends-now

    def percent_off(self):
        percentOff = ((self.original_price-self.deal_price)/self.original_price)*100
        return '{0:.2f}%'.format(percentOff)

    def price_pretty_print(input):
        return '${0:.2f}'.format(input)

    def __unicode__(self):
        return unicode(self.name)

class ClaimedDeal(models.Model):
    date_claimed = models.DateTimeField()
    deal_claimed = models.ForeignKey(Deal)
    user_claimed = models.ForeignKey(User)

    def __unicode__(self):
        return unicode(self.deal_claimed)






# Create your models here.
