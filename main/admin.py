from django.contrib import admin
from main.models import Venue, Deal, ClaimedDeal

#class VenueAdmin(admin.ModelAdmin):

class DealAdmin(admin.ModelAdmin):
    list_display = ('name','deal_price','original_price','date_created','date_deal_ends','percent_off')

class ClaimedDealAdmin(admin.ModelAdmin):
    list_display = ('deal_claimed', 'date_claimed')
#    fields = ('date_claimed','deal_claimed')

admin.site.register(Venue,)# VenueAdmin)
admin.site.register(Deal,DealAdmin)
admin.site.register(ClaimedDeal, ClaimedDealAdmin)