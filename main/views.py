# Create your views here.
from django.contrib import auth
from django.template import Template, Context
from django.template.loader import get_template
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from main import models
from django.contrib.auth import authenticate, login
from django import forms

def login(request):
    errors = []
    if request.method == 'POST':
        if not request.POST.get('email', ''):
            errors.append('Please enter your email.')
        if not request.POST.get('password', ''):
            errors.append('Please enter a password.')
        #additional authentication error username and/or password not found.

        if not errors:
            #submission successful redirect to login page:
            #email authentication and give login page a context indicating successful registration.
            return HttpResponseRedirect('/home/')

        #errors detected
        email = request.POST['email']
        password = request.POST['password']
        errorContext = RequestContext(request, {'errors':errors, 'email':email, 'password':password})
        errorRegistrationTemplate = get_template('html/login.html')
        htmlError = errorRegistrationTemplate.render(errorContext)
        return HttpResponse(htmlError)



    context = RequestContext(request, {})
    template = get_template('html/login.html')
    html = template.render(context)
    return HttpResponse(html)


def front(request):
    frontcontext = RequestContext(request, {'deallist10': models.Deal.objects.order_by("date_created")[0:10]})
    fronttemplate = get_template('html/front.html')
    html = fronttemplate.render(frontcontext)

    return HttpResponse(html)

def register(request):
    errors = []
    if request.method == 'POST':
        if not request.POST.get('name', ''):
            errors.append('Please enter a username.')
        if not request.POST.get('password', ''):
            errors.append('Please enter a password.')
        if not request.POST.get('password_confirm', '') :
            errors.append('Please confirm your password.')
        if request.POST.get('password_confirm', '') and request.POST.get('password', '') and (request.POST['password'] != request.POST['password_confirm']):
            errors.append('Password confirmation did not match the password you entered.')
        if not request.POST.get('email','') or (request.POST.get('email') and '@' not in request.POST['email']):
            errors.append('Enter a valid e-mail address.')
        if not request.POST.get('location','') or (request.POST.get('location', '') and request.POST['location'] == 'loc'):
            errors.append('Please select a location.')


        if not errors:
            #submission successful redirect to login page:
            #email authentication and give login page a context indicating successful registration.
            return HttpResponseRedirect('/registration_success')

        #errors detected
        name = request.POST['name']
        email = request.POST['email']
        location = request.POST['location']
        password = request.POST['password']
        errorContext = RequestContext(request, {'errors':errors, 'name':name, 'email':email, 'location':location, 'password':password})
        errorRegistrationTemplate = get_template('html/register.html')
        htmlError = errorRegistrationTemplate.render(errorContext)
        return HttpResponse(htmlError)

    #default rendering for form page
    registercontext = RequestContext(request,{})
    registertemplate = get_template('html/register.html')
    html = registertemplate.render(registercontext)

    return HttpResponse(html)


def registration_success(request):
    context = RequestContext(request,{})
    template = get_template('html/registration-success.html')
    html = template.render(context)
    return HttpResponse(html)