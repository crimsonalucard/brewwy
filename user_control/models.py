from django.db import models
from django.contrib.auth.models import User
from main.models import ClaimedDeal, Venue

class CustomerProfile(models.Model):
    user = models.OneToOneField(User)

    #custom user fields
    #should have NO PERMISSIONS
    neighborhood = models.CharField(max_length= 100, blank=True)
    region = models.CharField(max_length= 100, blank=True)
    city = models.CharField(max_length= 100, blank=True)
    state = models.CharField(max_length= 100, blank=True)
    country = models.CharField(max_length= 100, blank=True)
    county = models.CharField(max_length= 100, blank=True)
    zip = models.PositiveSmallIntegerField(max_length=5, blank=True)


class MerchantProfile(models.Model):
    user = models.OneToOneField(User)

    #custom merchant fields
    venue = models.ForeignKey(Venue)
    phone_number = models.PositiveSmallIntegerField(max_length = 10)
    neighborhood = models.CharField(max_length= 100, blank=True)
    region = models.CharField(max_length= 100, blank=True)
    city = models.CharField(max_length= 100, blank=True)
    state = models.CharField(max_length= 100, blank=True)
    country = models.CharField(max_length= 100, blank=True)
    county = models.CharField(max_length= 100, blank=True)
    zip = models.PositiveSmallIntegerField(max_length=5, blank=True)



# Create your models here.
